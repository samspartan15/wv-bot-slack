const { WebClient } = require('@slack/client');
const request = require('request-promise-native')
const token = process.env.SLACK_BOT_TOKEN;

const client = new WebClient(token);

const react = async (event) => {
  comment(event)
  return [
    await client.reactions.add({ name: 'west_virginia_logo', channel: event.channel, timestamp: event.ts}), 
    await client.reactions.add({ name: 'wv_state', channel: event.channel, timestamp: event.ts}), 
    await client.reactions.add({ name: 'mountaineer', channel: event.channel, timestamp: event.ts}), 
    await client.reactions.add({ name: 'coonskin', channel: event.channel, timestamp: event.ts}), 
  ]
}

const comment = async (event) => {
  if (event.subtype === 'file_share') {
    return await client.chat.postMessage({channel: event.channel, ts: event.ts, text: 'WEST VIRGINIA, BEST VIRGINIA'})
  }
}

const getImage = async url => {
  return await request({
    url,
    headers: {
      Authorization: `Bearer ${token}`
    }, 
    encoding: null
  })
}

exports.getImage = getImage;
exports.react = react;
