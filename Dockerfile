FROM node:8
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=production
# Bundle app source
COPY . .
EXPOSE 8000
CMD [ "npm", "start" ]
