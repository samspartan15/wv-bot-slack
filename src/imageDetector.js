const path = require('path')
const checker = require('./checker')
process.env.GOOGLE_APPLICATION_CREDENTIALS = path.resolve('./vision-key.json')
console.log(process.env.GOOGLE_APPLICATION_CREDENTIALS)
const vision = require('@google-cloud/vision')
const client = new vision.ImageAnnotatorClient()

const detect = async (options) => {
  return await client
  .annotateImage({
    image: options,
    features: [{ type: 'TEXT_DETECTION' }],
  })
  .then(results => {
    console.log(JSON.stringify(results))
    try {
      return !!results.find(result => 
        checker.check(result.fullTextAnnotation.text)
      );
    } catch (err) {
      console.log('no results for image')
    }
    return false
  })
  .catch(err => {
    console.error(err)
  })
}

exports.url = async url => {
  return await detect({ source: { imageUri: url } })
}

exports.buffer = async buffer => {
  return await detect({ content: buffer })
}
